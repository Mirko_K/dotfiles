" General settings
set noshowmode
set encoding=utf-8

" Save backups of files
set backupdir=~/.cache/vim/backup,~/tmp
set backup

" Enable persistent undo
set undodir=~/.cache/vim/undo
set undofile

" Indentation
set autoindent
set expandtab
set shiftround
set shiftwidth=2
set smarttab
set tabstop=2

" Search
set incsearch
set ignorecase
set smartcase

" Interface
set wildmenu
set number
set noerrorbells
set title

" Cycle through buffers with CTRL-h and CTRL-l
:nnoremap <C-h> :bp!<CR>
:nnoremap <C-l> :bn!<CR>
:nnoremap <C-c> :Bdelete<CR>

" Deoplete settings
let g:deoplete#enable_at_startup = 0
autocmd InsertEnter * call deoplete#enable()
let g:deoplete#sources#clang#libclang_path = '/usr/lib/llvm-3.9/lib/libclang.so'
let g:deoplete#sources#clang#clang_header = '/usr/lib/llvm-3.9/lib/clang/'
let g:deoplete#sources#clang#sort_algo = 'priority'
let g:deoplete#auto_complete_delay = 40

" Echodoc settings
set completeopt-=preview
let g:echodoc#enable_at_startup = 1

" ALE / Linter settings
let g:ale_sign_column_always = 1
let g:ale_cpp_clang_executable = 'clang++-3.9'
let g:ale_cpp_clangcheck_executable = 'clang-check-3.9'
let g:ale_linters = {'cpp': ['clang', 'clang_check']}

" Airline settings
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme = 'luna'

" NERDTree settings
let NERDTreeWinPos = 'right'
let NERDTreeWinSize = 40
let NERDTreeMinimalUI = 1
" Close nerdtree when it is the only window left
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" Open NERDTree when opening vim
autocmd vimenter * NERDTree | execute "wincmd h"

" Signify settings
let g:signify_vcs_list = [ 'git' ]

" Bind UndoTree to F5
nnoremap <F5> :UndotreeToggle<cr>

" Treat .h files as c++
autocmd BufRead,BufNewFile *.h set filetype=cpp

" Easymotion settings
map <Leader> <Plug>(easymotion-prefix)
nmap s <Plug>(easymotion-overwin-f2)

" Load plugins
call plug#begin('~/.vim/plugged')

  Plug 'flazz/vim-colorschemes'

  Plug 'w0rp/ale'

  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
  Plug 'zchee/deoplete-clang'

  Plug 'Shougo/echodoc.vim'

  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'

  Plug 'scrooloose/nerdtree'
  Plug 'moll/vim-bbye'

  Plug 'mhinz/vim-signify'

  Plug 'mbbill/undotree', { 'on':  'UndotreeToggle' }

  Plug 'easymotion/vim-easymotion'

call plug#end()

" Colors
set t_Co=256
colorscheme Benokai
syntax on
