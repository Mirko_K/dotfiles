#!/bin/bash

# Update to vim 8.0
sudo add-apt-repository ppa:jonathonf/vim
sudo apt update
sudo apt install vim vim-nox

# Make cache directory (used for backups)
mkdir -p ~/.cache/vim/backup/
mkdir -p ~/.cache/vim/undo/

# Install vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

# Install deoplete and ALE dependencies
sudo apt install python3-pip clang-3.9 libclang-3.9-dev
sudo pip3 install neovim
